require 'test_helper'

class AcquittancesControllerTest < ActionController::TestCase
  setup do
    @acquittance = acquittances(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:acquittances)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create acquittance" do
    assert_difference('Acquittance.count') do
      post :create, acquittance: { discount_value: @acquittance.discount_value, document_id: @acquittance.document_id, gross_value: @acquittance.gross_value, liquid_value: @acquittance.liquid_value, observation: @acquittance.observation, reference_code: @acquittance.reference_code }
    end

    assert_redirected_to acquittance_path(assigns(:acquittance))
  end

  test "should show acquittance" do
    get :show, id: @acquittance
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @acquittance
    assert_response :success
  end

  test "should update acquittance" do
    patch :update, id: @acquittance, acquittance: { discount_value: @acquittance.discount_value, document_id: @acquittance.document_id, gross_value: @acquittance.gross_value, liquid_value: @acquittance.liquid_value, observation: @acquittance.observation, reference_code: @acquittance.reference_code }
    assert_redirected_to acquittance_path(assigns(:acquittance))
  end

  test "should destroy acquittance" do
    assert_difference('Acquittance.count', -1) do
      delete :destroy, id: @acquittance
    end

    assert_redirected_to acquittances_path
  end
end
