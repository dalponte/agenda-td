Rails.application.routes.draw do

  resources :notifications , except: [:destroy, :edit, :update]
  resources :employees
  resources :activities
  resources :products
  resources :schedulings do
    collection do
      get 'todaypending'
      get 'today'
    end
  end

  resources :documents  do 
    collection do
      get 'pending'
      get 'payed'
      get 'with_received'
    end
  end  
  resources :acquittances do
    collection do
      get 'today'
    end
  end

  authenticated :user do
    resources :entities, only: [:new, :create, :edit, :update, :destroy]
    root to: 'entities#index', as: :authenticated_root
  end
  resources :entities, only: [:index, :show]
  root to: redirect('/users/sign_in')


  devise_for :users, 
  :controllers => { registrations: 'registrations' }, 
  :path_names => {:sign_up => 'register'}
  devise_scope :user do
    get "users/register(/:entity_id)", :to => "registrations#new", as: :new_entity_user
  end


  # controller :registrations do 
  # end
  controller :employees do 
    get 'employees/new(/:entity_id)' => :new, as: :new_entity_employee
  end
  controller :documents do 
    get 'documents/new(/:scheduling_id)' => :new, as: :new_scheduling_document
  end
  controller :acquittances do 
    get 'acquittances/new(/:document_id)' => :new, as: :new_document_acquittance
  end
  controller :notifications do 
    get 'schedulings/:scheduling_id/notifications/new' => :new, as: :new_scheduling_notification
    get 'notifications/:id/resend/' => :resend, as: :notification_resend
  end


end
