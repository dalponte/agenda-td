class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.references :entity, index: true, foreign_key: true
      t.string :function
      t.string :color
      t.boolean :active

      t.timestamps null: false
    end
  end
end
