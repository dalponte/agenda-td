class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.decimal :value
      t.string :description
      t.string :brand

      t.timestamps null: false
    end
  end
end
