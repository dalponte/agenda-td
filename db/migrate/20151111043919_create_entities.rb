class CreateEntities < ActiveRecord::Migration
  def change
    create_table :entities do |t|
      t.string :name
      t.string :email
      t.string :cpf
      t.string :adress
      t.string :phone

      t.timestamps null: false
    end
  end
end
