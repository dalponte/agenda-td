class CreateAcquittanceStatuses < ActiveRecord::Migration
  def change
    create_table :acquittance_statuses do |t|
      t.string :title
      t.string :description

      t.timestamps null: false
    end
  end
end
