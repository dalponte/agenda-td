class CreateDocumentActivities < ActiveRecord::Migration
  def change
    create_table :document_activities do |t|
      t.references :document, index: true, foreign_key: true
      t.references :activity, index: true, foreign_key: true
      t.references :employee, index: true, foreign_key: true
      t.integer :quantity
      t.decimal :unit_value
      t.date :realization
      t.time :beginning
      t.time :ending

      t.timestamps null: false
    end
  end
end
