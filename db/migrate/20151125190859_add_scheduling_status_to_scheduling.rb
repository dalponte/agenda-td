class AddSchedulingStatusToScheduling < ActiveRecord::Migration
  def change
    add_reference :schedulings, :scheduling_status, index: true, foreign_key: true
  end
end
