class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.references :entity, index: true, foreign_key: true
      t.references :scheduling, index: true, foreign_key: true
      t.integer :operation
      t.decimal :total_value
      t.decimal :discount_value
      t.string :observation

      t.timestamps null: false
    end
  end
end
