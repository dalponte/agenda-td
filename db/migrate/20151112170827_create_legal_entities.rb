class CreateLegalEntities < ActiveRecord::Migration
  def change
    create_table :legal_entities do |t|
      t.references :entity, index: true, foreign_key: true
      t.string :cnpj
      t.string :address
      t.string :phone

      t.timestamps null: false
    end
  end
end
