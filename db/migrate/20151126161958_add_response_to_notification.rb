class AddResponseToNotification < ActiveRecord::Migration
  def change
    add_column :notifications, :request_response, :int
  end
end
