class CreateComissions < ActiveRecord::Migration
  def change
    create_table :comissions do |t|
      t.references :employee, index: true, foreign_key: true
      t.references :activity, index: true, foreign_key: true
      t.decimal :value
      t.integer :percentage

      t.timestamps null: false
    end
  end
end
