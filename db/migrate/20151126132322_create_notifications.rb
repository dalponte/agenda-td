class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.references :entity, index: true, foreign_key: true
      t.references :scheduling, index: true, foreign_key: true
      t.integer :way
      t.string :ddd
      t.string :phone
      t.string :message, limit: 150
      t.string :email

      t.timestamps null: false
    end
  end
end
