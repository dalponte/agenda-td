class CreateSchedulingStatuses < ActiveRecord::Migration
  def change
    create_table :scheduling_statuses do |t|
      t.string :title
      t.string :description

      t.timestamps null: false
    end
  end
end
