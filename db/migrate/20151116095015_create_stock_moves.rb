class CreateStockMoves < ActiveRecord::Migration
  def change
    create_table :stock_moves do |t|
      t.references :product, index: true, foreign_key: true
      t.references :document, index: true, foreign_key: true
      t.integer :quantity

      t.timestamps null: false
    end
  end
end
