class CreateAcquittances < ActiveRecord::Migration
  def change
    create_table :acquittances do |t|
      t.references :document, index: true, foreign_key: true
      t.decimal :gross_value
      t.decimal :discount_value
      t.decimal :liquid_value
      t.string :reference_code
      t.string :observation

      t.timestamps null: false
    end
  end
end
