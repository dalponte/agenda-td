class CreateDocumentProducts < ActiveRecord::Migration
  def change
    create_table :document_products do |t|
      t.references :product, index: true, foreign_key: true
      t.references :document, index: true, foreign_key: true
      t.decimal :unit_value
      t.integer :quantity

      t.timestamps null: false
    end
  end
end
