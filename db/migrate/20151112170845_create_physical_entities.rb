class CreatePhysicalEntities < ActiveRecord::Migration
  def change
    create_table :physical_entities do |t|
      t.string :cpf
      t.string :address
      t.string :phone
      t.references :entity, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
