class CreateSchedulings < ActiveRecord::Migration
  def change
    create_table :schedulings do |t|
      t.references :entity, index: true, foreign_key: true
      t.references :activity, index: true, foreign_key: true
      t.references :employee, index: true, foreign_key: true
      t.datetime :dtt_moment
      t.time :t_end
      t.string :observation

      t.timestamps null: false
    end
  end
end
