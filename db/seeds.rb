
SchedulingStatus.create(title: 'Scheduled', description: "Scheduling registered and active")
SchedulingStatus.create(title: 'Canceled', description: "Scheduling canceled and time open")
SchedulingStatus.create(title: 'Notified', description: "The user was notified about the Scheduling")
SchedulingStatus.create(title: 'Consummate', description: "Document generated dor Scheduling")
