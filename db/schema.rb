# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151126211942) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "acquittance_statuses", force: :cascade do |t|
    t.string   "title"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "acquittances", force: :cascade do |t|
    t.integer  "document_id"
    t.decimal  "gross_value"
    t.decimal  "discount_value"
    t.decimal  "liquid_value"
    t.string   "reference_code"
    t.string   "observation"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "acquittances", ["document_id"], name: "index_acquittances_on_document_id", using: :btree

  create_table "activities", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.decimal  "value"
    t.integer  "duration"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "comissions", force: :cascade do |t|
    t.integer  "employee_id"
    t.integer  "activity_id"
    t.decimal  "value"
    t.integer  "percentage"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "comissions", ["activity_id"], name: "index_comissions_on_activity_id", using: :btree
  add_index "comissions", ["employee_id"], name: "index_comissions_on_employee_id", using: :btree

  create_table "document_activities", force: :cascade do |t|
    t.integer  "document_id"
    t.integer  "activity_id"
    t.integer  "employee_id"
    t.integer  "quantity"
    t.decimal  "unit_value"
    t.date     "realization"
    t.time     "beginning"
    t.time     "ending"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "document_activities", ["activity_id"], name: "index_document_activities_on_activity_id", using: :btree
  add_index "document_activities", ["document_id"], name: "index_document_activities_on_document_id", using: :btree
  add_index "document_activities", ["employee_id"], name: "index_document_activities_on_employee_id", using: :btree

  create_table "document_products", force: :cascade do |t|
    t.integer  "product_id"
    t.integer  "document_id"
    t.decimal  "unit_value"
    t.integer  "quantity"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "document_products", ["document_id"], name: "index_document_products_on_document_id", using: :btree
  add_index "document_products", ["product_id"], name: "index_document_products_on_product_id", using: :btree

  create_table "documents", force: :cascade do |t|
    t.integer  "entity_id"
    t.integer  "scheduling_id"
    t.integer  "operation"
    t.decimal  "total_value"
    t.decimal  "discount_value"
    t.string   "observation"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "documents", ["entity_id"], name: "index_documents_on_entity_id", using: :btree
  add_index "documents", ["scheduling_id"], name: "index_documents_on_scheduling_id", using: :btree

  create_table "employees", force: :cascade do |t|
    t.integer  "entity_id"
    t.string   "function"
    t.string   "color"
    t.boolean  "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "employees", ["entity_id"], name: "index_employees_on_entity_id", using: :btree

  create_table "entities", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "adress"
    t.string   "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "ddd"
  end

  create_table "legal_entities", force: :cascade do |t|
    t.integer  "entity_id"
    t.string   "cnpj"
    t.string   "address"
    t.string   "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "legal_entities", ["entity_id"], name: "index_legal_entities_on_entity_id", using: :btree

  create_table "notifications", force: :cascade do |t|
    t.integer  "entity_id"
    t.integer  "scheduling_id"
    t.integer  "way"
    t.string   "ddd"
    t.string   "phone"
    t.string   "message",          limit: 150
    t.string   "email"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "request_response"
  end

  add_index "notifications", ["entity_id"], name: "index_notifications_on_entity_id", using: :btree
  add_index "notifications", ["scheduling_id"], name: "index_notifications_on_scheduling_id", using: :btree

  create_table "physical_entities", force: :cascade do |t|
    t.string   "cpf"
    t.string   "address"
    t.string   "phone"
    t.integer  "entity_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "physical_entities", ["entity_id"], name: "index_physical_entities_on_entity_id", using: :btree

  create_table "products", force: :cascade do |t|
    t.string   "name"
    t.decimal  "value"
    t.string   "description"
    t.string   "brand"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "scheduling_statuses", force: :cascade do |t|
    t.string   "title"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "schedulings", force: :cascade do |t|
    t.integer  "entity_id"
    t.integer  "activity_id"
    t.integer  "employee_id"
    t.datetime "dtt_moment"
    t.time     "t_end"
    t.string   "observation"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "scheduling_status_id"
  end

  add_index "schedulings", ["activity_id"], name: "index_schedulings_on_activity_id", using: :btree
  add_index "schedulings", ["employee_id"], name: "index_schedulings_on_employee_id", using: :btree
  add_index "schedulings", ["entity_id"], name: "index_schedulings_on_entity_id", using: :btree
  add_index "schedulings", ["scheduling_status_id"], name: "index_schedulings_on_scheduling_status_id", using: :btree

  create_table "stock_moves", force: :cascade do |t|
    t.integer  "product_id"
    t.integer  "document_id"
    t.integer  "quantity"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "stock_moves", ["document_id"], name: "index_stock_moves_on_document_id", using: :btree
  add_index "stock_moves", ["product_id"], name: "index_stock_moves_on_product_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.integer  "entity_id"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.boolean  "admin"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "acquittances", "documents"
  add_foreign_key "comissions", "activities"
  add_foreign_key "comissions", "employees"
  add_foreign_key "document_activities", "activities"
  add_foreign_key "document_activities", "documents"
  add_foreign_key "document_activities", "employees"
  add_foreign_key "document_products", "documents"
  add_foreign_key "document_products", "products"
  add_foreign_key "documents", "entities"
  add_foreign_key "documents", "schedulings"
  add_foreign_key "employees", "entities"
  add_foreign_key "legal_entities", "entities"
  add_foreign_key "notifications", "entities"
  add_foreign_key "notifications", "schedulings"
  add_foreign_key "physical_entities", "entities"
  add_foreign_key "schedulings", "activities"
  add_foreign_key "schedulings", "employees"
  add_foreign_key "schedulings", "entities"
  add_foreign_key "schedulings", "scheduling_statuses"
  add_foreign_key "stock_moves", "documents"
  add_foreign_key "stock_moves", "products"
end
