json.extract! @acquittance, :id, :document_id, :gross_value, :discount_value, :liquid_value, :reference_code, :observation, :created_at, :updated_at
