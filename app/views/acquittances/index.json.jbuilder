json.array!(@acquittances) do |acquittance|
  json.extract! acquittance, :id, :document_id, :gross_value, :discount_value, :liquid_value, :reference_code, :observation
  json.url acquittance_url(acquittance, format: :json)
end
