json.array!(@schedulings) do |scheduling|
  json.extract! scheduling, :id, :entity_id, :activity_id, :employee_id, :dtt_moment, :t_end, :observation
  json.url scheduling_url(scheduling, format: :json)
end
