json.array!(@notifications) do |notification|
  json.extract! notification, :id, :entity_id, :scheduling_id, :way, :ddd, :phone, :message, :email
  json.url notification_url(notification, format: :json)
end
