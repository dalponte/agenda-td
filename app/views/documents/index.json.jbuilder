json.array!(@documents) do |document|
  json.extract! document, :id, :entity_id, :scheduling_id, :operation, :total_value, :discount_value, :observation
  json.url document_url(document, format: :json)
end
