json.array!(@entities) do |entity|
  json.extract! entity, :id, :name, :email, :cpf, :adress, :phone
  json.url entity_url(entity, format: :json)
end
