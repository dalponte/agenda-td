json.array!(@employees) do |employee|
  json.extract! employee, :id, :entity_id, :function, :color, :active
  json.url employee_url(employee, format: :json)
end
