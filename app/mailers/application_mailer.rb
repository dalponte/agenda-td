class ApplicationMailer < ActionMailer::Base
  default from: "noreply@trendhair.com"
end
