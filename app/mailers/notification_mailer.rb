class NotificationMailer < ApplicationMailer
  default :subject => 'Hello, Look this!'

  def notification(notification)
    @notification = notification
    puts YAML::dump(@notification.email)
    @url  = 'http://localhost:3000/'
    mail to: @notification.email
  end

  def scheduling_notification(notification)
    @entity = notification.entity
    @scheduling = notification.scheduling
    @message = notification.message
    @url  = 'http://localhost:3000/'
    mail(to: notification.email, subject: 'Você possuí uma atividade agendada')
  end

end
