module SchedulingsHelper
	def status_tag status
		unless status.blank?
			color = status_color status.id
			"<span class='label label-#{color}'>#{status.title}</span>".html_safe
		end
	end

	def status_color status_id
		case status_id # a_variable is the variable we want to compare
			when 1 
			 "success" 
			when 2  
				"danger"
			when 3  
			  "info"
			else 
			  "default"
			end
	end
end
