class Document < ActiveRecord::Base
  belongs_to :entity
  belongs_to :scheduling
  before_create :validate_values

  has_many :document_activities
  has_many :activities, through: :document_activities
  accepts_nested_attributes_for :document_activities, reject_if: :all_blank, allow_destroy: true
  has_many :document_products 
  has_many :products, through: :document_products
  accepts_nested_attributes_for :document_products, reject_if: :all_blank, allow_destroy: true

  has_many :acquittances 

  enum operation: {'Sale' => 1, 'Purchase' => 2}

  validates :entity_id, presence: true
  validates :total_value, presence: true, :numericality => {:greater_than => 0 }
  validates :discount_value, :numericality => {:less_than_or_equal_to => :total_value }

  default_scope { order(id: :desc) }
  scope :pending, ->{having(
      'SUM(acquittances.gross_value) < (documents.total_value - documents.discount_value) OR SUM(acquittances.gross_value) IS NULL')}
  scope :payed, ->{having(
      'SUM(acquittances.gross_value) >= (documents.total_value - documents.discount_value)')}
  scope :with_status, ->{select("documents.*", 
      "SUM(acquittances.gross_value) as received_value", 
      "COUNT(acquittances.document_id) as quantity").joins(
      "LEFT JOIN acquittances ON acquittances.document_id = documents.id").group("documents.id")}
=begin

SELECT "documents"."id", 
  documents.total_value - documents.discount_value as final_v, 
  SUM(acquittances.gross_value) as received_v, 
  COUNT(acquittances.document_id) as quantity 
FROM "documents" 
  INNER JOIN "acquittances" ON "acquittances"."document_id" = "documents"."id" 
GROUP BY documents.id

=end

  #scope :payed, ->{joins(:acquittances).where() group('documents.id')}

  def validate_values
    if not self.total_value > 0 
      self.total_value = 0
    end
  	if self.discount_value == nil
  		self.discount_value = 0;
  	elsif self.total_value - self.discount_value < 0
  		self.discount_value = self.total_value 
  	end
  end

  def load_from_scheduling(scheduling)
    self.scheduling = scheduling
    self.scheduling_id = scheduling.id
    self.entity_id = scheduling.entity_id
    self.discount_value = 0
    self.total_value = scheduling.activity.value

    docAct = DocumentActivity.new(
      activity_id: scheduling.activity_id, 
      employee_id: scheduling.employee_id,
      unit_value: scheduling.activity.value,
      quantity: 1
    )

    self.document_activities.append(docAct)
  end

  def scheduling_date
    if self.scheduling
      self.scheduling.dtt_moment.strftime('%d/%m/%Y %H:%I')
    else
      nil
    end 
  end

  def final_value
    self.total_value - self.discount_value unless self.new_record?
  end

  def get_pending_value 
    self.final_value - self.get_received_value unless self.new_record?
  end

  def get_received_value
    self.acquittances.sum(:gross_value) unless self.new_record?
  end
end
