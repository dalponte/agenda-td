class Comission < ActiveRecord::Base
  belongs_to :employee
  belongs_to :activity

  validates_presence_of :activity
end
