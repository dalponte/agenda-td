class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  belongs_to :entity

	validates :entity, :uniqueness => true, allow_nil: true
	validates :email, presence: true, :uniqueness => true

end
