class Scheduling < ActiveRecord::Base
  belongs_to :entity
  belongs_to :activity
  belongs_to :employee
  belongs_to :scheduling_status

  has_one :document
  has_many :notifications

  validate :picked_moment, on: :create
  validate :available_moment, on: :update

  validates :entity_id, presence: true
  validates :employee_id, presence: true
  validates :dtt_moment, presence: true, allow_blank: false

  scope :today, ->{where("DATE(dtt_moment) = DATE(NOW())")}
  scope :status, ->(status) {where("scheduling_status_id = ?", status)}
  scope :pending, ->{where("scheduling_status_id != 2 AND scheduling_status_id != 4")}

  def picked_moment
    errors.add(:dtt_moment, "Date already Picked") if Scheduling.where("employee_id = ? AND dtt_moment = ?", self.employee_id, self.dtt_moment).any?
  end
  def available_moment
		errors.add(:dtt_moment, "Date already Picked") if Scheduling.where("employee_id = ? AND dtt_moment = ? AND id <> ?", self.employee_id, self.dtt_moment, self.id).any?
  end

end
