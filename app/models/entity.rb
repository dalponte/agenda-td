class Entity < ActiveRecord::Base
	has_one :legal_entity
	has_one :physical_entity
	has_one :user
	has_many :notifications
	has_many :documents
	has_many :schedulings

	accepts_nested_attributes_for :legal_entity
	accepts_nested_attributes_for :physical_entity
end
