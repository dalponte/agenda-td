class DocumentActivity < ActiveRecord::Base
  belongs_to :document
  belongs_to :activity
  belongs_to :employee
end
