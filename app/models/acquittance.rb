class Acquittance < ActiveRecord::Base
  belongs_to :document

  validates :document_id, presence: true
  validates :gross_value, presence: true, :numericality => true
  validates :discount_value, :numericality => {:greater_than_or_equal_to => 0}
  validate :valid_value, on: [:create, :update]

  default_scope { order(id: :desc) }
  scope :today, ->{where("DATE(created_at) = DATE(NOW())")}

  def valid_value 
  	pending_value = self.document.get_pending_value
  	received_value = self.document.get_received_value
  	if pending_value <= 0 && self.gross_value >= 0
    	errors.add(:gross_value, 'This document already was full payed') 
    elsif received_value + self.gross_value < 0 
    	errors.add(:gross_value, "Can't reverse document with no acquittances") 
    elsif self.gross_value > pending_value
    	self.gross_value =  pending_value
    	self.liquid_value = self.gross_value - self.discount_value
    end	
	end

  def load_from_document(document)
    self.document = document
    self.gross_value = document.get_pending_value
    self.discount_value = 0	
    self.liquid_value = self.gross_value
  end

  def get_reversed 
    acquittance = Acquittance.new
    acquittance.document_id = self.document_id
    acquittance.gross_value = self.gross_value * -1 
    acquittance.discount_value = 0
    acquittance.liquid_value = acquittance.gross_value
    acquittance.observation = "Reverse acquittance"
    acquittance.reference_code = self.id
    return acquittance
  end
end
