class Activity < ActiveRecord::Base
	has_many :comissions
	has_many :employee, through: :comissions

	validates :name, :value, :duration, presence: true

	def final_value(employee_id = nil)
		if employee_id 
			Comission.find_by(employee_id: employee_id).value
		else
			self.value
		end
	end

end
