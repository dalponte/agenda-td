class Employee < ActiveRecord::Base
	belongs_to :entity
	has_many :comissions
	has_many :activities, through: :comissions

	accepts_nested_attributes_for :comissions, allow_destroy: true

	validates :function, presence: true
	validates :entity, on: :create, presence: true, :uniqueness => true
	validates :entity, presence: true, if: :entity_employee?
	# validates_associated :activities # Verify the method work

  def entity_employee?
  	errors.add(:entity_id, "This entity already are a employee") if self.persisted? and Employee.where("entity_id = #{self.entity_id} and id != #{self.id}").any?
  end

  def employee_name
  	self.entity.name
  end

end
