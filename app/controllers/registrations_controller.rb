class RegistrationsController < Devise::RegistrationsController
  # before_filter :configure_permitted_parameters, :only => [:create]
  prepend_before_filter :authenticate_scope!, only: [:edit, :update, :destroy]
  
  def new 
  	@user = User.new
  	if params[:entity_id]

  		@user.entity_id = params[:entity_id]
  		@user.email = @user.entity.email unless @user.entity.email.blank?
  	end
  end
  private

  def account_update_params
    params.require(:user).permit(:entity_id, :email, :password, :password_confirmation, :current_password)
  end
end