class DocumentsController < ApplicationController
  before_action :set_document, only: [:show, :edit, :update, :destroy]

  # GET /documents
  # GET /documents.json
  def index
    @documents = Document.with_status
    @active_all = "active"
  end
  def pending
    @documents = Document.with_status.pending
    @active_pending = "active"
    render action: :index
  end
  
  def payed
    @documents = Document.with_status.payed
    @active_payed = "active"
    render action: :index
  end
  def with_received
    @documents = Document.with_status
    render action: :with_received
  end

  # GET /documents/1
  # GET /documents/1.json
  def show
    respond_to do |format|
      format.html
      format.json {render json: @document, :include => :entity, :methods => [:final_value, :get_pending_value, :get_received_value], :root => false}
    end
  end

  # GET /documents/new
  def new
    @document = Document.new
    if params[:scheduling_id]
      @document.load_from_scheduling(Scheduling.find(params[:scheduling_id]))
    else
      @document.entity = Entity.new
    end
  end

  # GET /documents/1/edit
  def edit
  end

  # POST /documents
  # POST /documents.json
  def create
    @document = Document.new(document_params)

    respond_to do |format|
      if @document.save
        format.html { redirect_to @document, notice: 'Document was successfully created.' }
        format.json { render :show, status: :created, location: @document }
      else
        format.html { render :new }
        format.json { render json: @document.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /documents/1
  # PATCH/PUT /documents/1.json
  def update
    respond_to do |format|
      if @document.update(document_params)
        format.html { redirect_to @document, notice: 'Document was successfully updated.' }
        format.json { render :show, status: :ok, location: @document }
      else
        format.html { render :edit }
        format.json { render json: @document.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /documents/1
  # DELETE /documents/1.json
  def destroy
    @document.destroy
    respond_to do |format|
      format.html { redirect_to documents_url, notice: 'Document was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_document
      @document = Document.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def document_params
      params.require(:document).permit(:entity_id, :scheduling_id, :operation, :total_value, :discount_value, :observation, 
        document_activities_attributes: [:id, :activity_id, :employee_id, :quantity, :unit_value, :realization, :beginning, :ending, :_destroy], 
        document_products_attributes: [:id, :product_id, :quantity, :unit_value, :_destroy])
    end
end
