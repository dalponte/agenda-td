class AcquittancesController < ApplicationController
  before_action :set_acquittance, only: [:show, :edit, :update, :destroy]

  # GET /acquittances
  # GET /acquittances.json
  def index
    @acquittances = Acquittance.all
    @active_all = "active"
  end
  def today
    @acquittances = Acquittance.today
    @active_today = "active"
    render action: :index
  end

  # GET /acquittances/1
  # GET /acquittances/1.json
  def show
  end

  # GET /acquittances/new
  def new
    @acquittance = Acquittance.new
    if params[:document_id]
      @acquittance.load_from_document(Document.find(params[:document_id]))
    else
      @acquittance.document = Document.new
    end
    @pending = Document.pending
  end

  # GET /acquittances/1/edit
  def edit
  end

  # POST /acquittances
  # POST /acquittances.json
  def create
    @acquittance = Acquittance.new(acquittance_params)

    respond_to do |format|
      if @acquittance.save
        format.html { redirect_to @acquittance, notice: 'Acquittance was successfully created.' }
        format.json { render :show, status: :created, location: @acquittance }
      else
        format.html { render :new }
        format.json { render json: @acquittance.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /acquittances/1
  # PATCH/PUT /acquittances/1.json
  def update
    respond_to do |format|
      if @acquittance.update(acquittance_params)
        format.html { redirect_to @acquittance, notice: 'Acquittance was successfully updated.' }
        format.json { render :show, status: :ok, location: @acquittance }
      else
        format.html { render :edit }
        format.json { render json: @acquittance.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /acquittances/1
  # DELETE /acquittances/1.json
  def destroy
    @reverse = @acquittance.get_reversed 
    respond_to do |format|
      if @reverse.save
        format.html { redirect_to @reverse, notice: "The Acquittance #{@acquittance.id} was successfully reversed." }
        format.json { render :show, status: :created, location: @acquittance }
      else
        format.html { render :new }
        format.json { render json: @acquittance.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_acquittance
      @acquittance = Acquittance.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def acquittance_params
      params.require(:acquittance).permit(:document_id, :gross_value, :discount_value, :liquid_value, :reference_code, :observation)
    end
end
