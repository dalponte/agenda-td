class NotificationsController < ApplicationController
  before_action :set_notification, only: [:show, :resend]

  # GET /notifications
  # GET /notifications.json
  def index
    @notifications = Notification.all
  end

  # GET /notifications/1
  # GET /notifications/1.json
  def show
  end

  # GET /notifications/new
  def new
    @notification = Notification.new

    if params[:scheduling_id]
      @notification.scheduling_id = params[:scheduling_id]
      @notification.entity_id = @notification.scheduling.entity_id
      @notification.email = @notification.entity.email
      @notification.ddd = @notification.entity.ddd
      @notification.phone = @notification.entity.phone
      @notification.way = 2
    else
      @notification.entity = Entity.new
    end
  end

  # GET /notifications/1/edit
  def edit
  end

  # POST /notifications
  # POST /notifications.json
  def create
    @notification = Notification.new(notification_params)


    @notification.request_response = send_notification(@notification.scheduling_id)

    respond_to do |format|
      if @notification.save
        format.html { redirect_to @notification, notice: 'Notification was successfully created.' }
        format.json { render :show, status: :created, location: @notification }
      else
        format.html { render :new }
        format.json { render json: @notification.errors, status: :unprocessable_entity }
      end
    end
  end

  def resend 
    @notification.request_response = send_notification(@notification.scheduling_id)
    redirect_to @notification, :notice => 'It shoud be resended'
  end

  # PATCH/PUT /notifications/1
  # PATCH/PUT /notifications/1.json
  def update
    respond_to do |format|
      if @notification.update(notification_params)
        format.html { redirect_to @notification, notice: 'Notification was successfully updated.' }
        format.json { render :show, status: :ok, location: @notification }
      else
        format.html { render :edit }
        format.json { render json: @notification.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /notifications/1
  # DELETE /notifications/1.json
  def destroy
    @notification.destroy
    respond_to do |format|
      format.html { redirect_to notifications_url, notice: 'Notification was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  def send_sms(notification)
    require 'net/http'
    url = URI.parse('http://192.168.25.85/be/controller/SmsController.php')
      #url = URI.parse('http://192.168.25.85/be/view/teste.php')
      post_args = { 
        'option' => "SmsUnico",
        'message' => notification.message,
        'ddd' => notification.ddd,
        'number' => notification.phone,
        'email' => "dalpontemaico@gmail.com",
        'token' => "7d9856f9329847b0"
      }
      # send the request
      @resp, @data = Net::HTTP.post_form(url, post_args)
      puts YAML::dump(@resp)
      puts YAML::dump(@data)

      return @resp.body
  end
  def send_notification(scheduling_id)
    if @notification.way == 'SMS'  
      puts "------------- Sending SMS -----------------"
      return send_sms(@notification)
    elsif @notification.way == 'E-mail'
      puts "------------- Sending EMAIL -----------------"
      if scheduling_id.blank?
        return NotificationMailer.notification(@notification).deliver_now        
      else
        return NotificationMailer.scheduling_notification(@notification).deliver_now        
      end
    else
      return nil    
    end
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_notification
    @notification = Notification.find(params[:id])
  end

    # Never trust parameters from the scary internet, only allow the white list through.
    def notification_params
      params.require(:notification).permit(:entity_id, :scheduling_id, :way, :ddd, :phone, :message, :email)
    end
  end
